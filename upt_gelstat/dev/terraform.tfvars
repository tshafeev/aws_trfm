# Region specific parameter. Ensure you update these when setting up variable files for environments in different regions.
aws_region = "us-east-1"

# Remote State file location, predefined and set up by TM
terraform_bucket = "uptick.${var.aws_region}.tm-intl-dev"

remote_state_key = "uptick/dev/general_gelstat_dev.tfstate"

# Tags to be applied to all resources
environment_tag = "dev"

#zone = "dev.uptick.com"

vpc_id = "vpc-5f566d39"

aws_subnet = "subnet-6f20e827"

aws_iam_role = "gelstat_iam"

aws_iam_role_pol = "gelstat_iam_pol"
aws_iam_ins_pro          = "gelstat_profile"

gelstat_ecs {
  asg_min_size             = 1
  asg_max_size             = 1
  image_id                 = "ami-275ffe31"
  instance_type            = "t2.small"
  key_name                 = "uptick-dev"
  aws_autoscaling_group    = "gelstat_asg"
  aws_sg                   = "gelstat_sg"
  ecs_name                 = "gelstat_ecs"
  aws_launch_configuration = "gelstal_lc"
  aws_alb                  = "gelstat-alb"
  aws_ecs_service          = "gelstat_ecs_svc"
  
}

zone {
  route53_domain  = "dev.uptick.com"
  route53_zone_id = "ZJHG2UWU5CQ9I"
}

# Elastic Load Balancer
#ssl_certificate = "please_put_the_AWS_ARN_of_the_required_Certificate_Manager_certificate"

