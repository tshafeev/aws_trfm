#Authoring Task Definition and Service
data "template_file" "task-definition-gelstat-template" {
  template = "${file("${path.module}/ectd_gelstat.json")}"

  vars {
    image_url_gelstat = "${var.ecr_host}/${var.ecr_repo}:${var.environment_tag}-gelstat-${var.gelstat_image_version}"
    container_name    = "${var.environment_tag}-gelstat-${var.gelstat_image_version}"
    environment_name  = "${var.environment_tag}"
    log_group_region  = "${var.aws_region}"
    log_group_name    = "${aws_cloudwatch_log_group.cw-gelstat.name}"
  }
}

resource "aws_ecs_task_definition" "task-definition-gelstat" {
  family                = "${var.environment_tag}-gelstat-task-definition"
  container_definitions = "${data.template_file.task-definition-gelstat-template.rendered}"
}

resource "aws_ecs_service" "ecs-service-gelstat" {
  name            = "${var.gelstat_ecs["aws_ecs_service"]}-gelstat"
  cluster         = "${aws_ecs_cluster.main.id}"
  task_definition = "${aws_ecs_task_definition.task-definition-gelstat.arn}"
  desired_count   = 1
  iam_role        = "${aws_iam_role.ecs-service-role.name}"

  load_balancer {
    target_group_arn = "${aws_alb_target_group.target_group_gelstat.id}"
    container_name   = "${var.environment_tag}-gelstat-${var.gelstat_image_version}"
    container_port   = 8080
  }

  depends_on = [
    "aws_iam_role_policy.ecs-service-policy",
    "aws_alb_listener.listener_gelstat",
  ]
}
