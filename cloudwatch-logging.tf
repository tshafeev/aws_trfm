## CloudWatch Logs
resource "aws_cloudwatch_log_group" "cw-ecs" {
  name              = "uptick-awsdeint-ecs-group/ecs-agent"
  retention_in_days = "${var.logs_retention_period}"

  tags {
    Environment = "${var.environment_tag}"
  }
}

resource "aws_cloudwatch_log_group" "cw-gelstat" {
  name              = "uptick-awsdeint-ecs-group/uptick-gelstat"
  retention_in_days = "${var.logs_retention_period}"

  tags {
    Environment = "${var.environment_tag}"
  }
}
