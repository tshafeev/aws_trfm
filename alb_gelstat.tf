resource "aws_alb_target_group" "target_group_gelstat" {
  name     = "uptick_${var.environment_tag}-gelstat"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    path = "/health"
  }

  tags {
    Name        = "${var.gelstat_ecs["aws_alb"]}"
    Environment = "${var.environment_tag}"
  }
}

resource "aws_alb" "alb_gelstat" {
  name            = "${var.gelstat_ecs["aws_alb"]}-gelstat"
  subnets         = ["${var.aws_subnet}"]
  security_groups = ["${aws_security_group.sg-alb.id}"]

  tags {
    Name        = "${var.gelstat_ecs["aws_alb"]}"
    Environment = "${var.environment_tag}"
  }
}

resource "aws_alb_listener" "listener_gelstat" {
  load_balancer_arn = "${aws_alb.alb_gelstat.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_alb_target_group.target_group_gelstat.arn}"
    type             = "forward"
  }
}
