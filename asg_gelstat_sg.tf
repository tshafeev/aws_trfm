### Security
# Create a security group for ec2 instances
resource "aws_security_group" "sg-instance" {
  description = "Controls access to the ASG from ALB"

  name   = "${var.gelstat_ecs["aws_sgs"]}-instance"
  vpc_id = "${var.vpc_id}"

  lifecycle {
    create_before_destroy = true
  }

  # ingress {
  #   from_port       = 8080
  #   to_port         = 8080
  #   protocol        = "tcp"
  #   security_groups = ["${aws_security_group.sg-alb.id}"]
  # }

  ingress {
    from_port = 0
    to_port   = 65535
    protocol  = "tcp"

    ##        self = true
    security_groups = ["${aws_security_group.sg-alb.id}"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["193.201.205.0/24"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["86.57.255.88/29"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name        = "${var.gelstat_ecs["aws_sgs"]}"
    Environment = "${var.environment_tag}"
  }
}
