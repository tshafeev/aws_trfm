### IAM Roles and Policies
resource "aws_iam_role" "ecs-service-role" {
  name = "${var.aws_iam_role}ecs"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "ecs-service-policy" {
  name = "${var.aws_iam_role_pol}"
  role = "${aws_iam_role.ecs-service-role.name}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:Describe*",
        "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
        "elasticloadbalancing:DeregisterTargets",
        "elasticloadbalancing:Describe*",
        "elasticloadbalancing:DeleteListener",
        "elasticloadbalancing:DeleteLoadBalancer",
        "elasticloadbalancing:DeleteLoadBalancerListeners",
        "elasticloadbalancing:DeleteLoadBalancerPolicy",
        "elasticloadbalancing:DeleteRule",
        "elasticloadbalancing:DeleteTargetGroup",
        "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
        "elasticloadbalancing:RegisterTargets",
        "elasticloadbalancing:SetLoadBalancerListenerSSLCertificate",
        "elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer",
        "elasticloadbalancing:SetLoadBalancerPoliciesOfListener",
        "elasticloadbalancing:SetRulePriorities",
        "elasticloadbalancing:SetSecurityGroups",
        "elasticloadbalancing:SetSubnets"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "iam-inst-profile" {
  name  = "${var.aws_iam_ins_pro}"
  roles = ["${aws_iam_role.ec2-instance.name}"]
}

resource "aws_iam_role" "ec2-instance" {
  name = "${var.aws_iam_role}instance"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

data "template_file" "instance-profile" {
  template = "${file("${path.module}/instance-profile-policy.json")}"

  vars {
    gelstat_log_group_arn = "${aws_cloudwatch_log_group.cw-gelstat.arn}"
    ecs_log_group_arn     = "${aws_cloudwatch_log_group.cw-ecs.arn}"
  }
}

resource "aws_iam_role_policy" "ecs-instance" {
  name   = "${var.aws_iam_role_pol}"
  role   = "${aws_iam_role.ec2-instance.name}"
  policy = "${data.template_file.instance-profile.rendered}"
}
