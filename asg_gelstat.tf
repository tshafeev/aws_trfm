### Autoscaling Gelstat Service

resource "aws_autoscaling_group" "asg-gelstat" {
  name                 = "${var.gelstat_ecs["aws_ecs_ag"]}"
  min_size             = "${var.gelstat_ecs["asg_min_size"]}"
  max_size             = "${var.gelstat_ecs["asg_max_size"]}"
  launch_configuration = "${aws_launch_configuration.launch-gelstat.name}"
  vpc_zone_identifier  = ["${var.vpc_id}"]
  health_check_type    = "EC2"
  enabled_metrics      = ["GroupMinSize", "GroupMaxSize", "GroupDesiredCapacity", "GroupInServiceInstances", "GroupPendingInstances", "GroupStandbyInstances", "GroupTerminatingInstances", "GroupTotalInstances"]
}

resource "aws_autoscaling_policy" "asg-scale-down-gelstat" {
  name                    = "${var.gelstat_ecs["aws_ecs_ag"]}-scale-down-gelstat"
  metric_aggregation_type = "Average"
  policy_type             = "StepScaling"

  step_adjustment {
    scaling_adjustment          = -1
    metric_interval_lower_bound = 0.0
  }

  adjustment_type        = "ChangeInCapacity"
  autoscaling_group_name = "${aws_autoscaling_group.asg-gelstat.name}"
}

# Policies
resource "aws_autoscaling_policy" "asg-scale-up-gelstat" {
  name                      = "${var.gelstat_ecs["aws_ecs_ag"]}}-scale-up-gelstat"
  estimated_instance_warmup = 150
  metric_aggregation_type   = "Average"
  policy_type               = "StepScaling"

  step_adjustment {
    scaling_adjustment          = 40
    metric_interval_lower_bound = 0.0
  }

  adjustment_type        = "PercentChangeInCapacity"
  autoscaling_group_name = "${aws_autoscaling_group.asg-gelstat.name}"
}

resource "aws_cloudwatch_metric_alarm" "metric-memory-high-gelstat" {
  alarm_name          = "${var.environment_tag}-MemoryReservation-High-gelstat"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "MemoryReservation"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "75"
  alarm_description   = "This metric monitors ECS memory for High MemoryReservation on gelstat hosts"

  alarm_actions = [
    "${aws_autoscaling_policy.asg-scale-up-gelstat.arn}",
  ]

  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.asg-gelstat.name}"
  }

  actions_enabled = "true"
}

resource "aws_cloudwatch_metric_alarm" "metric-memory-low-gelstat" {
  alarm_name          = "${var.environment_tag}-MemoryReservation-Low-gelstat"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "MemoryReservation"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "25"
  alarm_description   = "This metric monitors ECS Low MemoryReservation on gelstat hosts"

  alarm_actions = [
    "${aws_autoscaling_policy.asg-scale-down-gelstat.arn}",
  ]

  dimensions {
    name  = "ClusterName"
    value = "${aws_ecs_cluster.main.id}"
  }

  actions_enabled = "true"
}

resource "aws_launch_configuration" "launch-gelstat" {
  security_groups = [
    "${aws_security_group.sg-instance.id}",
  ]

  # "${data.terraform_remote_state.vpc.sg_linux_common}",

  name                 = "${var.gelstat_ecs["aws_launch_configuration"]}"
  key_name             = "${var.gelstat_ecs["key_name"]}"
  image_id             = "${var.gelstat_ecs["image_id"]}"
  instance_type        = "${var.gelstat_ecs["instance_type"]}"
  iam_instance_profile = "${aws_iam_instance_profile.iam-inst-profile.name}"
  user_data = <<EOF
       #!/bin/bash
       echo ECS_CLUSTER=${aws_ecs_cluster.main.name} >> /etc/ecs/ecs.config
       EOF
  enable_monitoring = true
  lifecycle {
    create_before_destroy = true
  }
}
