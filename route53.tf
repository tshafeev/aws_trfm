resource "aws_route53_record" "r53-gelstat" {
  #zone_id = "ZJHG2UWU5CQ9I"
  zone_id = "${var.zone["route53_zone_id"]}"

  name = "gelstat-${var.environment_tag}.${var.zone["route53_domain"]}"

  #name    = "gelstat.${var.environment_tag}.${var.zone["route53_domain"}"
  type = "A"

  alias {
    name                   = "${lower(aws_alb.alb_gelstat.dns_name)}"
    zone_id                = "${aws_alb.alb_gelstat.zone_id}"
    evaluate_target_health = true
  }
}
