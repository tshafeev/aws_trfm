output "instance_security_group" {
  value = "${aws_security_group.sg-instance.id}"
}

output "launch_configuration_gelstat" {
  value = "${aws_launch_configuration.launch-gelstat.id}"
}

output "asg_name_gelstat" {
  value = "${aws_autoscaling_group.asg-gelstat.id}"
}

output "alb_hostname_gelstat" {
  value = "${aws_alb.alb_gelstat.dns_name}"
}
