# Create a security group for an ALB
resource "aws_security_group" "sg-alb" {
  description = "Controls access to the application ELB"

  lifecycle {
    create_before_destroy = true
  }

  name   = "${var.gelstat_ecs["aws_sgs"]}-alb"
  vpc_id = "${var.vpc_id}"

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["10.0.0.0/8"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["86.57.255.88/29"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["193.201.205.0/24"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  tags {
    Name        = "${var.gelstat_ecs["aws_sgs"]}-alb"
    Environment = "${var.environment_tag}"
  }
}
