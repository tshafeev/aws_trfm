# TAG and name

variable "environment_tag" {}

# variable "account_tag" {}
### Compute
variable "aws_region" {}

# Remote State file location, predefined and set up by TM
# variable "aws_access_key" {}

# variable "aws_secret_key" {}

variable "terraform_bucket" {}

variable "remote_state_key" {}
variable "vpc_id" {}
variable "aws_subnet" {}
variable "aws_iam_role" {}
variable "aws_iam_role_pol" {}
variable "aws_iam_ins_pro" {}

variable "gelstat_ecs" {
  type = "map"

  default = {
    # Autoscaling Group
    "asg_min_size"             = ""
    "asgmax_size"              = ""
    "image_id"                 = ""
    "instance_type"            = ""
    "aws_ecs_ag"               = ""
    "ecs_name"                 = ""
    "aws_launch_configuration" = ""
    "aws_alb"                  = ""
    "aws_sgs"                  = ""
  }
}

variable "zone" {
  type = "map"
  default = {
    "route53_zone_id" =""
    "route53_domain" =""
  }
}

### Autoscaling
variable "asg_min" {
  default = "1"
}

variable "asg_max" {
  default = "2"
}

# FIXME: clean up unnecessary stuff
variable "asg_desired" {
  description = "Desired numbers of servers in ASG"
  default     = "1"
}

variable "ami" {
  description = "AWS ECS AMI id"

  default = {
    us-east-1 = "ami-275ffe31"
  }
}

variable "ami_prefix" {
  default = "amazon"
}

variable "availability_zones" {
  default = ["us-east-1a", "us-east-1b"]
}

variable "ssl_certificate" {
  default = "none"
}

variable "ecr_host" {
  default = "harbor.upticktests.info/harbor/projects"
}

variable "ecr_repo" {
  default = "library"
}

variable "gelstat_image_version" {
  default = "latest"
}
